import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/model/card/card_information_item.dart';
import 'package:wone_app/model/member/member_information_item.dart';
import 'package:wone_app/pages/member/add_new_card_screen.dart';
import 'package:wone_app/pages/member/log_in_page.dart';

class ComponentMemberAll extends StatelessWidget {
  const ComponentMemberAll({
    super.key,
    required this.memberInformationItem,
    required this.cardInformationItem,
    required this.callback

  });
  final MemberInformationItem memberInformationItem;
  final CardInformationItem cardInformationItem;
  final VoidCallback callback;

  // DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언

  @override
  Widget build(BuildContext context) {
    // double mediaQueryWidth = MediaQuery.of(context).size.width;
    // double mediaQueryHeight = MediaQuery.of(context).size.height;
    //
    // return ListView(
    //   children: [
    //     Container(
    //       child: Column(
    //         children: [
    //           /** 프로필 아이콘 **/
    //           Container(
    //             margin: EdgeInsets.fromLTRB(0, 50, 0, 30),
    //             height: mediaQueryWidth * 0.25,
    //             width: mediaQueryWidth * 0.25,
    //             decoration: const BoxDecoration(
    //               shape: BoxShape.circle,
    //               color: colorPrimary,
    //             ),
    //             child: Icon(
    //               Icons.person,
    //               color: Colors.white,
    //               size: mediaQueryWidth * 0.18,
    //             ),
    //           ),
    //           /** 개인정보 내용 **/
    //           Container(
    //             margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
    //             width: mediaQueryWidth,
    //             height: 208,
    //             decoration: BoxDecoration(
    //               color: colorLightGrey,
    //               borderRadius: BorderRadius.all(
    //                 Radius.circular(boxRadius),
    //               ),
    //             ),
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.start,
    //               children: [
    //                 Container(
    //                   padding: EdgeInsets.fromLTRB(25, 15, 20, 0),
    //                   alignment: Alignment.center,
    //                   child: Row(
    //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                     crossAxisAlignment: CrossAxisAlignment.center,
    //                     children: [
    //                       /** 이름 입력 폼 **/
    //                       Container(
    //                         child: Text(
    //                           '이름',
    //                           style: TextStyle(
    //                             letterSpacing: fontLetterSpacing,
    //                             fontFamily: 'NotoSans_Bold',
    //                             fontSize: fontSizeMid,
    //                             color: colorLightBlack,
    //                           ),
    //                         ),
    //                       ),
    //                       /** 멤버 이름 **/
    //                       Container(
    //                         alignment: FractionalOffset.centerRight,
    //                         child: Row(
    //                           children: [
    //                             TextButton(
    //                               style: TextButton.styleFrom(
    //                                 minimumSize: Size.zero,
    //                                 padding: EdgeInsets.zero,
    //                               ),
    //                               onPressed: () {
    //                                 showDialog(
    //                                     context: context,
    //                                     barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
    //                                     builder: (BuildContext context) {
    //                                       return AlertDialog(
    //                                         content: TextField(
    //                                           style: TextStyle(
    //                                             fontSize: fontSizeMid,
    //                                             letterSpacing: fontLetterSpacing,
    //                                             color: colorLightBlack,
    //                                           ),
    //                                           decoration: InputDecoration(
    //                                             border: InputBorder.none,
    //                                             hintText: memberInformationItem.username,
    //                                             hintStyle: TextStyle(
    //                                               fontSize: fontSizeMid,
    //                                               letterSpacing: fontLetterSpacing,
    //                                               color: colorGrey,
    //                                             ),
    //                                           ),
    //                                         ),
    //                                         actions: [
    //                                           TextButton(
    //                                             child: const Text('취소'),
    //                                             onPressed: () {
    //                                               Navigator.of(context).pop();
    //                                             },
    //                                           ),
    //                                           TextButton(
    //                                             child: const Text('확인'),
    //                                             onPressed: () {
    //                                               Navigator.of(context).pop();
    //                                             },
    //                                           ),
    //                                         ],
    //                                       );
    //                                     }
    //                                 );
    //                               },
    //                               child: Text(memberInformationItem.username,
    //                                 style: TextStyle(
    //                                   letterSpacing: fontLetterSpacing,
    //                                   fontFamily: 'NotoSans_NotoSansKR-Regular',
    //                                   fontSize: fontSizeMid,
    //                                   color: colorDarkGrey,
    //                                 ),
    //                               ),
    //                             ),
    //                             Container(
    //                               child: Icon(
    //                                 Icons.chevron_right,
    //                                 color: colorDarkGrey,
    //                               ),
    //                             )
    //                           ],
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //                 /** 구분선 **/
    //                 Container(
    //                   margin: EdgeInsets.fromLTRB(0, 13, 0, 0),
    //                   height: 1,
    //                   color: colorGrey,
    //                 ),
    //                 Container(
    //                   padding: EdgeInsets.fromLTRB(25, 8, 20, 0),
    //                   alignment: Alignment.center,
    //                   child: Row(
    //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                     crossAxisAlignment: CrossAxisAlignment.center,
    //                     children: [
    //                       /** 생년월일 입력 폼 **/
    //                       Container(
    //                         child: Text(
    //                           '생년월일',
    //                           style: TextStyle(
    //                             letterSpacing: fontLetterSpacing,
    //                             fontFamily: 'NotoSans_Bold',
    //                             fontSize: fontSizeMid,
    //                             color: colorLightBlack,
    //                           ),
    //                         ),
    //                       ),
    //                       /** 멤버 생년월일 **/
    //                       Container(
    //                         alignment: FractionalOffset.centerRight,
    //                         child: Row(
    //                           children: [
    //                             TextButton(
    //                                 onPressed: () async{
    //                                   final selectedDate = await showDatePicker(
    //                                     context: context, // 팝업으로 띄우기 때문에 context 전달
    //                                     // initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
    //                                     firstDate: DateTime(1980), // 시작 년도
    //                                     lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
    //                                   );
    //                                   // if (selectedDate != null) {
    //                                   //   setState(() {
    //                                   //     date = selectedDate; // 선택한 날짜는 dart 변수에 저장
    //                                   //   });
    //                                   // }
    //                                 },
    //                                 style: TextButton.styleFrom(
    //                                   padding: EdgeInsets.zero,
    //                                 ),
    //                                 child: Row(
    //                                   mainAxisAlignment: MainAxisAlignment.end,
    //                                   crossAxisAlignment: CrossAxisAlignment.center,
    //                                   children: [
    //                                     // Text(
    //                                     //   // '$date'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
    //                                     //   // style: TextStyle(
    //                                     //   //   fontSize: fontSizeMid,
    //                                     //   //   letterSpacing: fontLetterSpacing,
    //                                     //   //   color: colorDarkGrey,
    //                                     //   // ),
    //                                     // ),
    //                                   ],
    //                                 )
    //                             ),
    //                             Container(
    //                               child: Icon(
    //                                 Icons.chevron_right,
    //                                 color: colorDarkGrey,
    //                               ),
    //                             )
    //                           ],
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //                 /** 구분선 **/
    //                 Container(
    //                   margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
    //                   height: 1,
    //                   color: colorGrey,
    //                 ),
    //                 Container(
    //                   padding: EdgeInsets.fromLTRB(25, 13, 20, 0),
    //                   alignment: Alignment.center,
    //                   child: Row(
    //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                     crossAxisAlignment: CrossAxisAlignment.center,
    //                     children: [
    //                       /** 비밀번호 입력 폼 **/
    //                       Container(
    //                         child: Text(
    //                           '비밀번호',
    //                           style: TextStyle(
    //                             letterSpacing: fontLetterSpacing,
    //                             fontFamily: 'NotoSans_Bold',
    //                             fontSize: fontSizeMid,
    //                             color: colorLightBlack,
    //                           ),
    //                         ),
    //                       ),
    //                       /** 멤버 비밀번호 **/
    //                       Container(
    //                         alignment: FractionalOffset.centerRight,
    //                         child: Row(
    //                           children: [
    //                             TextButton(
    //                               style: TextButton.styleFrom(
    //                                 minimumSize: Size.zero,
    //                                 padding: EdgeInsets.zero,
    //                               ),
    //                               onPressed: () {
    //                                 showDialog(
    //                                     context: context,
    //                                     barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
    //                                     builder: (BuildContext context) {
    //                                       return AlertDialog(
    //                                         content: TextField(
    //                                           style: TextStyle(
    //                                             fontSize: fontSizeMid,
    //                                             letterSpacing: fontLetterSpacing,
    //                                             color: colorLightBlack,
    //                                           ),
    //                                           decoration: InputDecoration(
    //                                             border: InputBorder.none,
    //                                             hintText: memberInformationItem.password,
    //                                             hintStyle: TextStyle(
    //                                               fontSize: fontSizeMid,
    //                                               letterSpacing: fontLetterSpacing,
    //                                               color: colorGrey,
    //                                             ),
    //                                           ),
    //                                         ),
    //                                         actions: [
    //                                           TextButton(
    //                                             child: const Text('취소'),
    //                                             onPressed: () {
    //                                               Navigator.of(context).pop();
    //                                             },
    //                                           ),
    //                                           TextButton(
    //                                             child: const Text('확인'),
    //                                             onPressed: () {
    //                                               Navigator.of(context).pop();
    //                                             },
    //                                           ),
    //                                         ],
    //                                       );
    //                                     }
    //                                 );
    //                               },
    //                               child: Text(memberInformationItem.password,
    //                                 style: TextStyle(
    //                                   letterSpacing: fontLetterSpacing,
    //                                   fontFamily: 'NotoSans_NotoSansKR-Regular',
    //                                   fontSize: fontSizeMid,
    //                                   color: colorDarkGrey,
    //                                 ),
    //                               ),
    //                             ),
    //                             Container(
    //                               child: Icon(
    //                                 Icons.chevron_right,
    //                                 color: colorDarkGrey,
    //                               ),
    //                             )
    //                           ],
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //                 /** 구분선 **/
    //                 Container(
    //                   margin: EdgeInsets.fromLTRB(0, 13, 0, 0),
    //                   height: 1,
    //                   color: colorGrey,
    //                 ),
    //                 Container(
    //                   padding: EdgeInsets.fromLTRB(25, 8, 20, 0),
    //                   alignment: Alignment.center,
    //                   child: Row(
    //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                     crossAxisAlignment: CrossAxisAlignment.center,
    //                     children: [
    //                       /** 카드 입력 폼 **/
    //                       Container(
    //                         child: Text(
    //                           '카드',
    //                           style: TextStyle(
    //                             letterSpacing: fontLetterSpacing,
    //                             fontFamily: 'NotoSans_Bold',
    //                             fontSize: fontSizeMid,
    //                             color: colorLightBlack,
    //                           ),
    //                         ),
    //                       ),
    //                       /** 멤버 카드 **/
    //                       Container(
    //                         alignment: FractionalOffset.centerRight,
    //                         child: Row(
    //                           children: [
    //                             TextButton(
    //                               onPressed: () {
    //                                 showDialog(
    //                                     context: context,
    //                                     barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
    //                                     builder: (BuildContext context) {
    //                                       return AlertDialog(
    //                                         content: Text(
    //                                           '현재 사용 중인 카드를\n변경하시겠습니까?',
    //                                           style: TextStyle(
    //                                             fontSize: fontSizeMid,
    //                                             letterSpacing: fontLetterSpacing,
    //                                             color: colorLightBlack,
    //                                           ),
    //                                         ),
    //                                         actions: [
    //                                           TextButton(
    //                                             child: const Text('취소'),
    //                                             onPressed: () {
    //                                               Navigator.of(context).pop();
    //                                             },
    //                                           ),
    //                                           TextButton(
    //                                             child: const Text('확인'),
    //                                             onPressed: () {
    //                                               Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddNewCardScreen()));
    //                                             },
    //                                           ),
    //                                         ],
    //                                       );
    //                                     }
    //                                 );
    //                               },
    //                               child: Row(
    //                                 mainAxisAlignment: MainAxisAlignment.end,
    //                                 crossAxisAlignment: CrossAxisAlignment.center,
    //                                 children: [
    //                                   Text(
    //                                     // '농협'
    //                                     cardInformationItem.cardGroup,
    //                                     style: TextStyle(
    //                                       letterSpacing: fontLetterSpacing,
    //                                       fontFamily: 'NotoSans_NotoSansKR-Regular',
    //                                       fontSize: fontSizeMid,
    //                                       color: colorDarkGrey,
    //                                     ),
    //                                   ),
    //                                 ],
    //                               ),
    //                             ),
    //                             Container(
    //                               child: Icon(
    //                                 Icons.chevron_right,
    //                                 color: colorDarkGrey,
    //                               ),
    //                             )
    //                           ],
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //           Container(
    //             alignment: FractionalOffset.center,
    //             margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
    //             width: mediaQueryWidth,
    //             height: mediaQueryHeight * 0.075,
    //             decoration: BoxDecoration(
    //               color: colorLightGrey,
    //               borderRadius: BorderRadius.all(
    //                 Radius.circular(boxRadius),
    //               ),
    //             ),
    //             child: Container(
    //               width: mediaQueryWidth,
    //               child: TextButton(
    //                 onPressed: () {
    //                   showDialog(
    //                       context: context,
    //                       barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
    //                       builder: (BuildContext context) {
    //                         return AlertDialog(
    //                           content: Text(
    //                             '정말 로그아웃 하시겠습니까?',
    //                             style: TextStyle(
    //                               fontFamily: 'NotoSans_NotoSansKR-Regular',
    //                               fontSize: fontSizeMid,
    //                               letterSpacing: fontLetterSpacing,
    //                               color: colorLightBlack,
    //                             ),
    //                           ),
    //                           actions: [
    //                             TextButton(
    //                               child: const Text('취소'),
    //                               onPressed: () {
    //                                 Navigator.of(context).pop();
    //                               },
    //                             ),
    //                             TextButton(
    //                               child: const Text('확인'),
    //                               onPressed: () {
    //                                 Navigator.of(context).push(MaterialPageRoute(builder: (context) => LogInPage()));
    //                               },
    //                             ),
    //                           ],
    //                         );
    //                       }
    //                   );
    //                 },
    //                 style: TextButton.styleFrom(
    //                   padding: EdgeInsets.zero,
    //                 ),
    //                 child: Text(
    //                   '로그아웃 하기',
    //                   style: TextStyle(
    //                     letterSpacing: fontLetterSpacing,
    //                     fontFamily: 'NotoSans_NotoSansKR-Regular',
    //                     fontSize: fontSizeSm,
    //                     color: colorDarkGrey,
    //                   ),
    //                 ),
    //               ),
    //             ),
    //           ),
    //           /** 회원탈퇴하기 **/
    //           Container(
    //             alignment: FractionalOffset.center,
    //             margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
    //             width: mediaQueryWidth,
    //             height: mediaQueryHeight * 0.075,
    //             decoration: BoxDecoration(
    //               color: colorLightGrey,
    //               borderRadius: BorderRadius.all(
    //                 Radius.circular(boxRadius),
    //               ),
    //             ),
    //             child: Container(
    //               width: mediaQueryWidth,
    //               child: TextButton(
    //                 onPressed: () {
    //                   showDialog(
    //                       context: context,
    //                       barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
    //                       builder: (BuildContext context) {
    //                         return AlertDialog(
    //                           content: Text(
    //                             '정말 회원탈퇴 하시겠습니까?',
    //                             style: TextStyle(
    //                               fontFamily: 'NotoSans_NotoSansKR-Regular',
    //                               fontSize: fontSizeMid,
    //                               letterSpacing: fontLetterSpacing,
    //                               color: colorLightBlack,
    //                             ),
    //                           ),
    //                           actions: [
    //                             TextButton(
    //                               child: const Text('취소'),
    //                               onPressed: () {
    //                                 Navigator.of(context).pop();
    //                               },
    //                             ),
    //                             TextButton(
    //                               child: const Text('확인'),
    //                               onPressed: () {
    //                                 Navigator.of(context).pop();
    //                               },
    //                             ),
    //                           ],
    //                         );
    //                       }
    //                   );
    //                 },
    //                 style: TextButton.styleFrom(
    //                   padding: EdgeInsets.zero,
    //                 ),
    //                 child: Text(
    //                   '회원탈퇴 하기',
    //                   style: TextStyle(
    //                     letterSpacing: fontLetterSpacing,
    //                     fontFamily: 'NotoSans_NotoSansKR-Regular',
    //                     fontSize: fontSizeSm,
    //                     color: colorDarkGrey,
    //                   ),
    //                 ),
    //               ),
    //             ),
    //           ),
    //         ],
    //       ),
    //     ),
    //   ],
    // );
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Text(
            '${memberInformationItem.id}',
          ),
          Text(
            memberInformationItem.username,
          ),
          Text(
            '${memberInformationItem.birthDate}'
          ),
          Text(
              memberInformationItem.password
          ),
          Text(
              cardInformationItem.cardGroup
          ),
          Text(
              cardInformationItem.cardNumber
          ),
          Text(
              cardInformationItem.endDate
          ),
        ],
      ),
    );
  }
}
