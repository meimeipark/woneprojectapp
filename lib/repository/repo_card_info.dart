import 'package:dio/dio.dart';
import 'package:wone_app/config/config_api.dart';
import 'package:wone_app/model/card/card_info_list_result.dart';
import 'package:wone_app/model/card/card_info_result.dart';

class RepoCardInfo{

  Future<CardInfoListResult> getCard(num memberId) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/card/all/member-id/{memberId}}';

    final response = await dio.get(
        _baseUrl.replaceAll('{memberId}', memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return CardInfoListResult.fromJson(response.data);
  }

}