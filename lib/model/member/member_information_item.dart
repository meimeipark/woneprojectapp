import 'package:pattern_formatter/date_formatter.dart';
import 'package:intl/intl.dart';

class MemberInformationItem {
  num id;
  String username;
  String birthDate;
  String password;
  // String cardGroup;
  // String cardNumber;
  // String endDate;
  // String etcMemo;

  // MemberInformationItem(this.id, this.username, this.birthDate, this.password, this.cardGroup, this.cardNumber, this.endDate, this.etcMemo);
  MemberInformationItem(this.id, this.username, this.birthDate, this.password);

  factory MemberInformationItem.fromJson(Map<String, dynamic> json) {
    // DateTime birthDate = DateTime.parse(json['birth_date']);
    // String birthDateString = DateFormat('yyyy-MM-DD').format(birthDate);
    // print(birthDateString);

    return MemberInformationItem(
        json['id'],
        json['username'],
        json['birthDate'],
        json['password'],
        // json['cardGroup'],
        // json['cardNumber'],
        // json['endDate'],
        // json['etcMemo']
    );
  }
}