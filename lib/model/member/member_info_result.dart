/**
 * 멤버 인포 단수용 큰 틀
 */

import 'package:wone_app/model/member/member_info_response.dart';

class MemberInfoResult {
  String msg;
  num code;
  MemberInfoResponse data;

  MemberInfoResult(this.msg, this.code, this.data);

  factory MemberInfoResult.fromJson(Map<String, dynamic> json) {
    return MemberInfoResult(
        json['msg'],
        json['code'],
        MemberInfoResponse.fromJson(json['data'])
    );
  }
}