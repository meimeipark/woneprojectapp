/**
 * 멤버 인포 단수용 작은 틀
 */

class MemberInfoResponse {
  num id;
  String userId;
  String username;
  String birthDate;
  String password;
  String joinDate;
  String memberGroup;
  String memberStatus;


  MemberInfoResponse(
      this.id,
      this.userId,
      this.username,
      this.birthDate,
      this.password,
      this.joinDate,
      this.memberGroup,
      this.memberStatus
      );

  factory MemberInfoResponse.fromJson(Map<String, dynamic> json) {
    return MemberInfoResponse(
        json['id'],
        json['userId'],
        json['username'],
        json['birthDate'],
        json['password'],
        json['joinDate'],
        json['memberGroup'],
        json['memberStatus']
    );
  }
}