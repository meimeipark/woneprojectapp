// 고객센터 단수용 큰틀

import 'package:wone_app/model/answer/answer_response.dart';

class AnswerContentsResult {
  String msg;
  num code;
  AnswerResponse data;

  AnswerContentsResult(this.msg, this.code, this.data);

  factory AnswerContentsResult.fromJson(Map<String, dynamic> json) {
    return AnswerContentsResult(
        json['msg'],
        json['code'],
        AnswerResponse.fromJson(json['data'])
    );
  }
}