class CardInfoResponse {
  num id;
  num memberId;
  String cardGroup;
  String cardNumber;
  String endDate;
  num cvc;
  String etcMemo;

  CardInfoResponse(this.id, this.memberId, this.cardGroup, this.cardNumber, this.endDate, this.cvc, this.etcMemo);

  factory CardInfoResponse.fromJson(Map<String, dynamic> json){
    return CardInfoResponse(
        json['id'],
        json['memberId'],
        json['cardGroup'],
        json['cardNumber'],
        json['endDate'],
        json['cvc'],
        json['etcMemo']
    );
  }
}