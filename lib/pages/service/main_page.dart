import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_path.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/pages/service/spending_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({
    super.key,
  });


  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: Colors.white,
        shadowColor: Colors.black.withOpacity(appbarShadow),
        elevation: 2,
        title: Image.asset(
          '${mainLogo}',
          height: 50,
          width: 50,
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: _buildBody(context),
      resizeToAvoidBottomInset: false, // 키보드 올라왔을 때 화면 깨짐 현상 없애줌.
    );
  }

  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            // 진행 게이지
            Container(
              margin: EdgeInsets.fromLTRB(0, 30, 0, 10),
              alignment: FractionalOffset.center,
              child: Column(
                children: <Widget>[
                  CircularStepProgressIndicator(
                      totalSteps: 100, // 전체 진행도 (바꾸면 에러남)
                      currentStep: 75, // 진행도
                      stepSize: 25, // 게이지 안쪽굵기
                      selectedColor: colorPrimary,
                      unselectedColor: Colors.grey[200],
                      padding: 0,
                      width: mediaQueryWidth * 0.7,
                      height: mediaQueryWidth * 0.7,
                      selectedStepSize: 25, // 게이지 굵기
                      roundedCap: (_, __) => true, // 게이지 둥글게
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              child: Text(
                                'TODAY',
                                style: TextStyle(
                                    fontFamily: 'NotoSans_Bold',
                                    fontSize: fontSizeSuper,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorPrimary
                                ),
                              ),
                            ),
                            Container(
                              child: TextButton(
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          content: TextFormField(
                                            keyboardType: TextInputType.number, // 키보드 기본
                                            inputFormatters: [
                                              FilteringTextInputFormatter.digitsOnly, // 숫자만 입력 받고 싶을 때
                                              CurrencyTextInputFormatter(
                                                symbol: '', // 화폐단위
                                              )
                                            ],
                                            decoration: InputDecoration(hintText: '목표금액을 설정해 주세요.',
                                            hintStyle: TextStyle(
                                              color: colorGrey,
                                              fontSize: 14,
                                              letterSpacing: fontLetterSpacing,
                                            ),
                                            ),
                                          ),
                                          actions: [
                                            TextButton(
                                              child: const Text('취소',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  letterSpacing: fontLetterSpacing,
                                                ),
                                              ),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                            TextButton(
                                              child: const Text('확인',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  letterSpacing: fontLetterSpacing,
                                                ),
                                              ),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ],
                                        );
                                      }
                                  );
                                }, child: Text(
                                '22,500',
                                  // overflow: TextOverflow.ellipsis,
                                textScaleFactor: 1.0,
                                style: TextStyle(
                                    fontFamily: 'NotoSans_Bold',
                                    fontSize: 40,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorPrimary
                                ),
                              ),
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    '/',
                                    style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeSuper,
                                      color: colorGrey,
                                    ),
                                  ),
                                  Text(
                                      ' '
                                  ),
                                  Text(
                                    '30,000',
                                    style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeSuper,
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey,
                                    ),
                                  ),
                                ],
                              )
                            ),
                          ],
                        ),
                      )
                  ),
                ],
              ),
            ),
            // 목표 금액
            Container(
              width: 85,
              height: mediaQueryWidth * 0.07,
              margin: EdgeInsets.fromLTRB(0, 3, 0, 0),
              child: ElevatedButton(
                  style: OutlinedButton.styleFrom(
                    backgroundColor: colorPrimary,
                    foregroundColor: Colors.white,
                    padding: EdgeInsets.zero,
                  ),
                  onPressed: () {
                    showDialog(
                        context: context,
                        barrierDismissible: true, // 바깥 영역 터치시 닫을지 여부
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: TextFormField(
                              keyboardType: TextInputType.number, // 키보드 기본
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly, // 숫자만 입력 받고 싶을 때
                              ],
                              decoration: InputDecoration(hintText: '목표금액을 설정해 주세요.',
                                hintStyle: TextStyle(
                                  color: colorGrey,
                                  fontSize: fontSizeSm,
                                  letterSpacing: fontLetterSpacing,
                                ),
                              ),
                            ),
                            actions: [
                              TextButton(
                                child: const Text('취소',
                                  style: TextStyle(
                                    fontSize: fontSizeSm,
                                    letterSpacing: fontLetterSpacing,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              TextButton(
                                child: const Text('확인',
                                  style: TextStyle(
                                    fontSize: fontSizeSm,
                                    letterSpacing: fontLetterSpacing,
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        }
                    );
                  },
                  child: Text(
                    '목표금액',
                    style: TextStyle(
                        fontSize: fontSizeMid,
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_Bold'
                    ),
                  )
              ),
            ),
            // 리스트 추가, 제거 버튼
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(20, 5, 20, 0),
                  width: 80,
                  height: 25,
                  child: ElevatedButton(
                      style: OutlinedButton.styleFrom(
                          backgroundColor: colorGrey,
                          foregroundColor: Colors.white,
                          // minimumSize: Size.zero,
                          padding: EdgeInsets.zero,
                      ),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => SpendingPage()));
                      },
                      child: Text(
                        '관리',
                        style: TextStyle(
                            letterSpacing: fontLetterSpacing,
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSizeMicro
                        ),
                      )
                  ),
                ),
              ],
            ),
            // 지출 리스트
            Container(
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.fromLTRB(20, 10, 20, 5),
                      width: mediaQueryWidth,
                      height: mediaQueryWidth * 0.14,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12.withOpacity(0.06),
                              spreadRadius: 10,
                              blurRadius: 30,
                              offset: Offset(0,0)
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: mediaQueryWidth * 0.5,
                            child: Row(
                              children: [
                                // 시간
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 5, 0),
                                  child: Text(
                                    '08:00',
                                    style: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeMicro,
                                      color: colorGrey,
                                    ),
                                  ),
                                ),
                                // 카테고리 아이콘
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Image.asset("assets/images/category/05_bus.png",
                                    height: 38,
                                    width: 38,
                                  ),
                                ),
                                // 지출 내용
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Text(
                                    '대중교통',
                                    style: TextStyle(
                                      color: colorDark,
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMid,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // 지출 금액
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            alignment: FractionalOffset.centerRight,
                            width: mediaQueryWidth * 0.33,
                            child: Text(
                              '1,450',
                              style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: fontSizeMid,
                                color: colorGrey,
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                      width: mediaQueryWidth,
                      height: mediaQueryWidth * 0.14,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12.withOpacity(0.06),
                              spreadRadius: 10,
                              blurRadius: 30,
                              offset: Offset(0,0)
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: mediaQueryWidth * 0.5,
                            child: Row(
                              children: [
                                // 시간
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 5, 0),
                                  child: Text(
                                    '08:30',
                                    style: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeMicro,
                                      color: colorGrey,
                                    ),
                                  ),
                                ),
                                // 카테고리 아이콘
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Image.asset("assets/images/category/04_cafe.png",
                                    height: 38,
                                    width: 38,
                                  ),
                                ),
                                // 지출 내용
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Text(
                                    '커피',
                                    style: TextStyle(
                                      color: colorDark,
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMid,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // 지출 금액
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            alignment: FractionalOffset.centerRight,
                            width: mediaQueryWidth * 0.33,
                            child: Text(
                              '4,500',
                              style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: fontSizeMid,
                                color: colorGrey,
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
