import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_path.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/model/answer/answer_response.dart';
import 'package:wone_app/pages/answer/answer_write_page.dart';
import 'package:wone_app/repository/repo_faq_answer.dart';

class AnswerContentsPage extends StatefulWidget {
  const AnswerContentsPage({
    super.key,
    required this.id
  });

  final num id;

  @override
  State<AnswerContentsPage> createState() => _AnswerContentsPageState();
}

class _AnswerContentsPageState extends State<AnswerContentsPage> {
  AnswerResponse? _answerContents;

  Future<void> _loadAnswerContents() async {
    await RepoFaqAnswer().getAnswer(widget.id)
        .then((res) => {
      setState(() {
        _answerContents = res.data;
      })
    });
  }

  void initState(){
    super.initState();
    _loadAnswerContents();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "고객센터",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarFontSize,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.search_outlined,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_answerContents == null) { /** _answerContents이 null이면 **/
      return Text('데이터 로딩중..'); /** 데이터 로딩중 띄워줌 **/
    } else { /** 있으면 아래 내용 그려줘 **/
      return SingleChildScrollView(
        child: Column(
          children: [
            /** 고객 문의 내용 **/
            Container(
              padding: EdgeInsets.fromLTRB(20, 15, 20, 0),
              child: Column(
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                       Container(
                         child: Row(
                           children: [
                             /** 프로필 아이콘 **/
                             Container(
                               margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                               width: 40,
                               height: 40,
                               decoration: BoxDecoration(
                                 color: Color.fromRGBO(236, 236, 236, 1.0),
                                 borderRadius: BorderRadius.circular(20),
                               ),
                               child: Icon(
                                 Icons.person,
                                 color: Colors.white,
                                 size: 20,
                               ),
                             ),
                             /** 멤버 이름 **/
                             Container(
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   Container(
                                     child: Text(_answerContents!.memberName,
                                       style: TextStyle(
                                           fontSize: fontSizeMid,
                                           fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                           letterSpacing: fontLetterSpacing,
                                           color: colorLightBlack
                                       ),
                                     ),
                                   ),
                                   /** 작성 시간 **/
                                   Container(
                                     child: Text(_answerContents!.askCreateDate,
                                       textAlign: TextAlign.start,
                                       style: TextStyle(
                                           fontSize: fontSizeSm,
                                           fontFamily: 'NotoSans_NotoSansKR-Regular',
                                           letterSpacing: fontLetterSpacing,
                                           color: colorGrey
                                       ),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           ],
                         ),
                       ),
                        /** 수정 아이콘 버튼 **/
                        Container(
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                IconButton(onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => AnswerWritePage()));
                                },
                                  icon: Icon(Icons.edit,
                                    color: Color.fromRGBO(220, 220, 220, 1.0),
                                  ),
                                ),
                              ],
                            )
                        )
                      ],
                    ),
                  ),
                  /** 문의 내용 **/
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Text(_answerContents!.askContent,
                      style: TextStyle(
                          fontSize: fontSizeSm,
                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                          letterSpacing: fontLetterSpacing,
                          color: colorLightBlack
                      ),
                    ),
                  ),
                ],
              ),
            ),
            /** 구분선 **/
            Container(
              margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
              height: 1,
              color: colorLightGrey,
            ),
            /** 문의 답변 **/
            Container(
             padding: EdgeInsets.fromLTRB(20, 15, 20, 0),
              child: Column(
                children: [
                  Container(
                    child: Row(
                      children: [
                        /** 프로필 아이콘 **/
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(236, 236, 236, 1.0),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Icon(
                            Icons.person,
                            color: Colors.white,
                            size: 20,
                          ),
                        ),
                        /** 관리자 이름 **/
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text('Wone',
                                  style: TextStyle(
                                      fontSize: fontSizeMid,
                                      fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                      letterSpacing: fontLetterSpacing,
                                      color: colorLightBlack
                                  ),
                                ),
                              ),
                              /** 작성 시간 **/
                              Container(
                                child: Text(_answerContents!.askCreateDate,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: fontSizeSm,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  /** 문의 답변 **/
                  Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      alignment: FractionalOffset.centerLeft,
                      child: Column(
                        children: [
                          Text(
                            '${_answerContents?.comment}',
                            style: TextStyle(
                                fontSize: fontSizeSm,
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                letterSpacing: fontLetterSpacing,
                                color: colorLightBlack
                            ),
                          ),
                        ],
                      )
                  )
                ],
              ),
            )
          ],
        ),
      );
    }
  }
}
