import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';

class GuidePage extends StatefulWidget {
  const GuidePage({super.key});

  @override
  State<GuidePage> createState() => _GuidePageState();
}

class _GuidePageState extends State<GuidePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "이용방법",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarFontSize,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: ListView(
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container( // 타이틀
                  margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: Text(
                    '시작하기',
                  style: TextStyle(
                    fontSize: fontSizeBig,
                    color: colorLightBlack,
                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                    letterSpacing: fontLetterSpacing
                  ),
                  ),
                ),
                Container( // 내용
                  margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                  child: Text(
                    'WONE은 체크카드를 이용하는 소비 관리 서비스입니다.\n'
                        'WONE을 이용하기 위해서는 계정을 생성하여 카드 등록\n'
                        '절차를 진행해야 합니다.',
                    style: TextStyle(
                        fontSize: fontSizeSm,
                        color: colorGrey,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 구분선
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  height: 1,
                  color: colorLightGrey,
                ),
                Container( // 타이틀
                  margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: Text(
                    '로그인하기',
                    style: TextStyle(
                        fontSize: fontSizeBig,
                        color: colorLightBlack,
                        fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 내용
                  margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                  child: Text(
                    'WONE에 로그인하기 위해서는 회원가입 절차를 진행하여\n'
                        '계정을 생성해야 합니다.',
                    style: TextStyle(
                        fontSize: fontSizeSm,
                        color: colorGrey,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 구분선
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  height: 1,
                  color: colorLightGrey,
                ),
                Container( // 타이틀
                  margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: Text(
                    '프로필',
                    style: TextStyle(
                        fontSize: fontSizeBig,
                        color: colorLightBlack,
                        fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 내용
                  margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                  child: Text(
                    '[≡] 아이콘을 클릭해, 내 프로필을 확인하고 정보를\n'
                        '수정할 수 있습니다.',
                    style: TextStyle(
                        fontSize: fontSizeSm,
                        color: colorGrey,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 구분선
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  height: 1,
                  color: colorLightGrey,
                ),
                Container( // 타이틀
                  margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: Text(
                    '메인화면',
                    style: TextStyle(
                        fontSize: fontSizeBig,
                        color: colorLightBlack,
                        fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 내용
                  margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                  child: Text(
                    '당일 챌린지 상태를 확인할 수 있습니다.',
                    style: TextStyle(
                        fontSize: fontSizeSm,
                        color: colorGrey,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 구분선
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  height: 1,
                  color: colorLightGrey,
                ),
                Container( // 타이틀
                  margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: Text(
                    '설정',
                    style: TextStyle(
                        fontSize: fontSizeBig,
                        color: colorLightBlack,
                        fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 내용
                  margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                  child: Text(
                    '[톱니바퀴] 아이콘을 클릭해, 원하는 설정 메뉴를 선택합니다.',
                    style: TextStyle(
                        fontSize: fontSizeSm,
                        color: colorGrey,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 구분선
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  height: 1,
                  color: colorLightGrey,
                ),
                Container( // 타이틀
                  margin: EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: Text(
                    '알림',
                    style: TextStyle(
                        fontSize: fontSizeBig,
                        color: colorLightBlack,
                        fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 내용
                  margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
                  child: Text(
                    '알림 사용 여부를 설정할 수 있습니다.',
                    style: TextStyle(
                        fontSize: fontSizeSm,
                        color: colorGrey,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        letterSpacing: fontLetterSpacing
                    ),
                  ),
                ),
                Container( // 구분선
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  height: 1,
                  color: colorLightGrey,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
